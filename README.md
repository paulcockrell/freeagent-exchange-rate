# Exchangerate

Designed to use ECB exchange rate XML, which are found here http://www.ecb.europa.eu/stats/exchange/eurofxref/html/index.en.html
and provide an easy way to obtain the exchange rate for given currencies

## Clone source
```shell
$> git clone git@bitbucket.org:paulcockrell/freeagent-exchange-rate.git
```

## Run tests
```shell
$> bundle exec rspec spec --format documentation
```
## Usage

Add this line to your application's Gemfile:

```ruby
gem 'exchangerate'
```

And then execute:

```shell
$> bundle
```

Or install it yourself as:
```shell
$> gem install exchangerate
```

## Example

To use you must first download the ECB xml file:

```shell
$> wget http://www.ecb.europa.eu/stats/eurofxref/eurofxref-hist-90d.xml > /tmp/ecb.xml
```

In your code configure Exchange rate to use this file as its source, and then call its processing function to return exchange rate.

```ruby
require 'exchangerate'

Exchangerate.configure do |config|
  config.source = "/tmp/ecb.xml"
end

exchange_rate = Exchangerate.at("2015-09-23", "USD", "GBP")
```

If your source file is updated on the file system, Exchangerate will automatically detect them and reload the source at the next processing request.
You can turn off this behaviour by using the following configuration option:

```ruby
Exchangerate.configure do |config|
  config.source = "/tmp/ecb.xml"
  config.auto_reload = false
end
```

## Development

After checking out the repo, run `bin/setup` to install dependencies. Then, run `bin/console` for an interactive prompt that will allow you to experiment.

To install this gem onto your local machine, run `bundle exec rake install`. To release a new version, update the version number in `version.rb`, and then run `bundle exec rake release` to create a git tag for the version, push git commits and tags, and push the `.gem` file to [rubygems.org](https://rubygems.org).
