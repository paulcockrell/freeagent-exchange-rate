require "spec_helper"
require "nokogiri"

describe Exchangerate do
  let(:source_file_path) { File.join("spec", "support", "source_file.xml") }
  let(:source_file_path_not_found) { File.join("does", "not", "exist.xml") }
  let(:source_file_path_invalid) { 0 } 
  let(:source_file_malformed) { File.join("spec", "support", "source_file_malformed.xml") }

  describe ".source = source_file" do
  end

  describe ".file" do
    it "Raise error when source is not set" do
      Exchangerate.configure do |config|
        config.source = nil
      end
      expect{Exchangerate.file}.to raise_error(Exchangerate::SourceNil)
    end

    it "Raise error when source file parameter path not found" do
      Exchangerate.configure do |config|
        config.source = source_file_path_not_found
      end
      expect{Exchangerate.file}.to raise_error(Exchangerate::SourceNotFound)
    end

    it "Raise error when source file parameter path invalid" do
      Exchangerate.configure do |config|
        config.source = source_file_path_invalid
      end
      expect{Exchangerate.file}.to raise_error(Exchangerate::SourceInvalid)
    end

    it "Raise error when source file malformed" do
      Exchangerate.configure do |config|
        config.source = source_file_malformed
      end
      expect{Exchangerate.file}.to raise_error(Exchangerate::FileMalformed)
    end

    it "Passes when valid source file parameter provided" do
      Exchangerate.configure do |config|
        config.source = source_file_path
      end

      expect(Exchangerate.file.errors).to eq([])
    end

    it "Reloads file if file changed and configure.auto_reload == true" do
      Exchangerate.configure do |config|
        config.auto_reload = true # true by default
        config.source = source_file_path
      end
      Exchangerate.file # force the file to be loaded
      system("touch #{source_file_path}")
      expect(Exchangerate.source_file_changed?).to eq(true)
    end

    it "Doesn't reload file if changed and configure.auto_reload == false" do
      Exchangerate.configure do |config|
        config.auto_reload = false
        config.source = source_file_path
      end
      Exchangerate.file # force the file to be loaded
      system("touch #{source_file_path}")
      expect(Exchangerate.source_file_changed?).to eq(false)
    end

    it "Doesn't reload file if NOT changed and configure.auto_reload == true" do
      Exchangerate.configure do |config|
        config.auto_reload = true
        config.source = source_file_path
      end
      Exchangerate.file # force the file to be loaded
      expect(Exchangerate.source_file_changed?).to eq(false)
    end
  end

  describe ".at(Date, base_currency_code, counter_currency_code)" do
    let(:date) { "2015-09-23" }
    let(:from) { "USD" }
    let(:to)   { "GBP" }

    it "Passes with valid parameters" do 
      Exchangerate.configure do |config|
        config.source = source_file_path
      end
      expect(Exchangerate.at(date, from, to)).to eq(0.5)
    end

    it "Raise params error with invalid date parameter" do 
      Exchangerate.configure do |config|
        config.source = source_file_path
      end
      ["01-01-1979", "", 2, nil].each do |bad_date|
        expect{Exchangerate.at(bad_date, from, to)}.to raise_error(Exchangerate::ParamsError)
      end
    end

    it "Raise params error with invalid from parameter" do 
      Exchangerate.configure do |config|
        config.source = source_file_path
      end
      [:USD, "", 2, nil].each do |bad_from|
        expect{Exchangerate.at(date, bad_from, to)}.to raise_error(Exchangerate::ParamsError)
      end
    end

    it "Raise params error with invalid to parameter" do 
      Exchangerate.configure do |config|
        config.source = source_file_path
      end
      [:USD, "", 2, nil].each do |bad_to|
        expect{Exchangerate.at(date, from, bad_to)}.to raise_error(Exchangerate::ParamsError)
      end
    end
  end
end
