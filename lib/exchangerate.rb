require "exchangerate/version"
require "nokogiri"

module Exchangerate
  class SourceError < StandardError; end
  class FileError < StandardError; end
  class ParamsError < StandardError; end
  class SourceNil < SourceError; end
  class SourceNotFound < SourceError; end
  class SourceInvalid < SourceError; end
  class FileMalformed < FileError; end
  class DateInvalid < ParamsError; end
  class CurrencyCodeInvalid < ParamsError; end

  class << self
    attr_accessor :configuration, :_file, :_file_stat_ctime
  end

  def self.configure
    self.configuration ||= Configuration.new
    self._file = nil
    self._file_stat_ctime = nil

    yield self.configuration
  end

  class Configuration
    attr_accessor :source, :auto_reload

    def initialize
      @source = nil
      @auto_reload = true
    end
  end

  def self.file
    self._file = nil if self._file && source_file_changed?
    return self._file unless self._file.nil?

    begin
      if valid_source? 
        File.open(self.configuration.source) do |f|
          self._file = ::Nokogiri::XML(f) { |config| config.strict }
          self._file_stat_ctime = f.stat.ctime
        end
        self._file.remove_namespaces!
      end
    rescue Nokogiri::XML::SyntaxError => e
      self._file = nil
      raise FileMalformed, e
    end

    self._file
  end

  def self.at(date, from, to)
    exchange_rates = load_exchange_rates_for(date, from, to)

    begin
      ( exchange_rates[:to_rate] / exchange_rates[:from_rate]).round(2)
    rescue Exception => e
      raise "Failed to perform rates conversion: #{e.message}"
    end
  end

  private

  def self.load_exchange_rates_for(date, from, to)
    exchange_rate_cube = self.file.xpath("//Cube/Cube[@time='#{date}']/Cube")
    raise ParamsError, "Date #{date} not found" if exchange_rate_cube.nil? || exchange_rate_cube.empty?

    exchange_rates = {}.tap do |rates|
      exchange_rate_cube.each do |el|
        rates[:from_rate] = el.xpath("@rate").text.to_f if el.xpath("@currency").text == from
        rates[:to_rate] = el.xpath("@rate").text.to_f if el.xpath("@currency").text == to
      end
    end

    errors = []
    errors << "Currency code #{from} not found" if exchange_rates[:from_rate] == nil
    errors << "Currency code #{to} not found" if exchange_rates[:to_rate] == nil
    if errors.empty?
      exchange_rates
    else
      raise ParamsError, errors.join(", ")
    end
  end

  def self.valid_source?
    raise SourceNil, "You must specify a source file" if self.configuration.source.nil?
    raise SourceInvalid, "You must provide a string representing a path to a source file" unless self.configuration.source.class == String
    raise SourceNotFound, "You must provice a valid path to a source file" unless File.exist? self.configuration.source

    true
  end

  def self.source_file_changed?
    return false unless self.configuration.auto_reload == true

    self._file_stat_ctime != File.stat(self.configuration.source).ctime
  end
end
